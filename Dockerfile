FROM wting/lambdabot

RUN mkdir -p $HOME/.lambdabot/State
COPY lambdaserv /lambdaserv
EXPOSE 8012
CMD /lambdaserv
